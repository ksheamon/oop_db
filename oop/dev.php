<?php

include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
include_once 'mainFunctions.php';
sec_session_start();
 
 

?>

<html>
<head>
<script type="text/JavaScript" src="js/sha512.js"></script> 
<script type="text/JavaScript" src="js/forms.js"></script> 
<style type="text/css" rel="stylesheet">
.container{
	width:1325px;
	margin:auto;
} form{
	display:inline-block;
	padding:10px 0 10px 10px;
	border-radius:10px;
	background-color:#009ddc;
	float:left;
	width:200px;
}.contentarea{
	float:right;
	width:900px;
}.question{
	cursor: pointer;
	color:#000;
}.question:before{
	clear:both;
} .answer {
   	margin: 0px 3px 3px;
   	height: auto;
    display: none;
    padding: 5px 20px;
 }.answer img, .HOD img, .leaders img{
	display:block;
 }.leaders ul{
	list-style:none;
 }.leaders ul li{
	display: inline-block;
    float: left;
    padding: 0 50px 50px 0;
    width: 350px;
 }.leaders ol{
	display:block;
	padding-left:0;
 }.leaders ol li{
	display:block;
	padding:0 0 10px 20px;
	font-style:italic;
	line-height:1;
 }.leaders ol li:before{
	content:'- ';
 }.leaders .admin{
	padding: 0 0 50px 25px;
 }.menu2{
	position:relative;  
    width:200px;
	top:10px;
	padding:16px;
    z-index:100
 }#login{
	background-color:#fff;
	border-radius:10px;
	height:300px;
	width:300px;
	position:absolute;
	top:10%;
	left:20%;
	z-index:999;
	border:2px solid #000;
	padding:30px 15px;
 }.error{
	color:#f00;
 }.menu{
	background-color: #009DDC;
    border-radius: 20px 20px 0 0;
    height: 70px;
    margin-top: 10px;
    padding: 30px 10px 0;
    position: relative;
    width: 1365px;
    z-index: 1;
}.menu a{
	color: #FFFFFF;
    font-size: 18px;
    padding: 0 20px;
    text-decoration: none;
}.menu a:hover{
	text-decoration:underline;
}.dataArea{
	background-color: #F1F1F1;
    border-radius: 0 0 20px 20px;
    margin-top: -20px;
    padding: 30px;
    position: relative;
    width: 1325px;
    z-index: 2;
}
</style>
</head>
	<body>
		<div class="container">
			<header>
				<div>
					<a href="http://127.0.0.1/oop"><img alt="Blue Fountain Media" src="http://www.bluefountainmedia.com/images/logo-desctop.png" width="360" height="60"></a>
				</div>
				<div>
					<a href="#" onclick="showLogin()">Login</a>
				</div>
			</header>
			

	<div class='menu'>
		<a href="/oop/admin/leadership.php">Leadership</a>
		<a href="/oop/admin/development.php">Development</a>
		<a href="/oop/admin/design.php">Design</a>
		<a href="/oop/admin/marketing.php">Marketing</a>
		<a href="/oop/admin/human_resources.php">Human Resources</a>
		<a href="/oop/admin/finance.php">Finance</a>
		<a href="/oop/admin/sales.php">BC/Sales</a>
		<a href="/oop/admin/accounts.php">Accounts</a>
		<a href="/oop/admin/creative_strategy.php">Creative Strategy</a>
		<a href="/oop/admin/information_technology.php">Information Technology</a>
	</div>

		<div class="dataArea">	
			
			
			
				<div class="menu2" id="floatdiv">
					<form method="POST" action=''>
						<label>Departments</label><br/>
						<input type="radio" checked="checked" value="LeadershipDept" name="department"/>Leadership<br/>
						<input type="radio" value="DevDept" name="department"/>Development<br/>
						<input type="radio" value="DesignDept" name="department"/>Design<br/>
						<input type="radio" value="MarketingDept" name="department"/>Marketing<br/>
						<input type="radio" value="HRDept" name="department"/>Human Resources<br/>
						<input type="radio" value="FinanceDept" name="department"/>Finance<br/>
						<input type="radio" value="SalesDept" name="department"/>BC/Sales<br/>
						<input type="radio" value="AccountsDept" name="department"/>Accounts<br/>
						<input type="radio" value="CSDept" name="department"/>Creative Strategy<br/>
						<input type="radio" value="ITDept" name="department"/>Information Technology<br/>
						<a href="#"><input type="submit" name="button1" value="View Department"/></a>
					</form>
				</div>
				<div class="contentarea">
					<?php run('DevDept');?>
				</div>
				<br style="clear:both;"/>
		
		</div>
		<?php
					if (isset($_GET['error'])) {?>
					
				<div class="login" id="login">
				<p class="error">Error Logging In!</p>
			<form action="includes/process_login.php" method="post" name="login_form">                      
				Email: <input type="text" name="email" /><br/>
				Password: <input type="password" 
                             name="password" 
                             id="password"/><br/>
				<input type="button" 
                   value="Login" 
                   onclick="formhash(this.form, this.form.password);" /> 
			</form>
		<?php } else { ?>
			
		<div class="login" id="login" style="display:none;">
				
			<form action="includes/process_login.php" method="post" name="login_form">                      
				Email: <input type="text" name="email" /><br/>
				Password: <input type="password" 
                             name="password" 
                             id="password"/><br/>
				<input type="button" 
                   value="Login" 
                   onclick="formhash(this.form, this.form.password);" /> 
			</form>

			</div>
		<?php } ?>
		</div>
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="java.js"></script>
<script type="text/javascript">
function showLogin(){
	document.getElementById('login').style.display="block";
}
</script>
	</body>
</html>