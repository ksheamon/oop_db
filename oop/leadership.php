<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
include_once 'includes/mainFunctions.php';
sec_session_start();
 
 if (login_check($mysqli) == true) {
    $logged = 'in';
} else {
    $logged = 'out';
}


?>
<html>
<head>
<link href="css/style.css" rel="stylesheet" type="text/css"/>
</head>
	<body>
		<div class="container">
			<?php include('includes/header.php');?>
			

	<div class='menu'>
		<a href="/oop/leadership.php">Leadership</a>
		<a href="/oop/development.php">Development</a>
		<a href="/oop/design.php">Design</a>
		<a href="/oop/marketing.php">Marketing</a>
		<a href="/oop/human_resources.php">Human Resources</a>
		<a href="/oop/finance.php">Finance</a>
		<a href="/oop/sales.php">BC/Sales</a>
		<a href="/oop/accounts.php">Accounts</a>
		<a href="/oop/creative_strategy.php">Creative Strategy</a>
		<a href="/oop/information_technology.php">Information Technology</a>
	</div>

		<div class="dataArea">			
				<div class='sidebar'>
					<h1>Leadership Team</h1>
					<div class="menu2" id="floatdiv">
						<div class="desc">
							<?php echo getDesc('LeadershipDept');?>
						</div>
					</div>
				</div>
				<div class="contentarea">
					<?php run('LeadershipDept');?>
				</div>
				<br style="clear:both;"/>
		
		</div>
		<?php
					if (isset($_GET['error'])) {
				
					?>
					
				<div class="login" id="login">
				<p class="error">Error Logging In!</p>
			<form action="includes/process_login.php" method="post" name="login_form">                      
				Email: <input type="text" name="email" /><br/>
				Password: <input type="password" 
                             name="password" 
                             id="password"/><br/>
				<input type="button" 
                   value="Login" 
                   onclick="formhash(this.form, this.form.password);" /> 
			</form>
			</div>
		<a href="javascript:void(0);" onclick="hide()"><div id="overlay"></div></a>
		<?php } else { ?>
			
		<div class="login" id="login" style="display:none;">
				
			<form action="includes/process_login.php" method="post" name="login_form">                      
				Email: <input type="text" name="email" /><br/>
				Password: <input type="password" 
                             name="password" 
                             id="password"/><br/>
				<input type="button" 
                   value="Login" 
                   onclick="formhash(this.form, this.form.password);" /> 
			</form>

			</div>
			</div>
		<a href="javascript:void(0);" onclick="hide()"><div id="overlay" style="display:none;"></div></a>
		<?php } ?>
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="js/java.js"></script>
<script type="text/JavaScript" src="js/sha512.js"></script> 
	</body>
</html>