<?php
include 'includes/db_connect.php';
include 'includes/functions.php';
include 'includes/AdminFunctions.php';

 
sec_session_start();
 
 if (login_check($mysqli) == true) {
    $logged = 'in';
} else {
    $logged = 'out';
}

// Check connection
if (mysqli_connect_errno()) {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
}


?>
<html>
<head>
<link rel="stylesheet" href="css/style.css" type="text/css"/>
<link rel="stylesheet" href="css/admin.css" type="text/css"/>
</head>
<body>

<div class="container">
<?php include('includes/header.php');?>

<div class='block'>
<div class='menu'>
	<a href="/oop/admin/leadership.php">Leadership</a>
	<a href="/oop/admin/development.php">Development</a>
	<a href="/oop/admin/design.php">Design</a>
	<a href="/oop/admin/marketing.php">Marketing</a>
	<a href="/oop/admin/human_resources.php">Human Resources</a>
	<a href="/oop/admin/finance.php">Finance</a>
	<a href="/oop/admin/sales.php">BC/Sales</a>
	<a href="/oop/admin/accounts.php">Accounts</a>
	<a href="/oop/admin/creative_strategy.php">Creative Strategy</a>
	<a href="/oop/admin/information_technology.php">Information Technology</a>
</div>

<div class="dataArea">
	<?php display('FinanceDept');?>


<?php function printDeptData(){ ?>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
	<input type="text" name="ID" value="<?php echo $n;?>"  class="readonly" readonly>
	<input type="text" name="Title" value="<?php getTitle($n);?>">
	<input type="text" name="Name" value="<?php getName($n);?>">
	<input type="text" name="Photo" value="<?php getPhoto($n);?>">
	<input type="text" name="Desc" value="<?php getDesc($n);?>">
	<textarea name="Resp"><?php getResp($n);?></textarea>
	<input type="submit" name="update" value="update"/><br/>
</form>
<hr/>

<?php } ?>




</div>
</div>
</div>

<script>

</script>

</body>
</html>