<?php
include 'includes/db_connect.php';
include 'includes/functions.php';
include 'includes/AdminFunctions.php';
 include_once 'includes/leadershipFunctions.php';
 
sec_session_start();
 
 if (login_check($mysqli) == true) {
    $logged = 'in';
} else {
    $logged = 'out';
}

// Check connection
if (mysqli_connect_errno()) {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
}


?>
<html>
<head>
<link rel="stylesheet" href="css/style.css" type="text/css"/>
<link rel="stylesheet" href="css/admin.css" type="text/css"/>

</head>
<body>

<div class="container">
<?php include('includes/header.php');?>

<div class='block'>
<div class='menu'>
	<a href="/oop/admin/leadership.php">Leadership</a>
	<a href="/oop/admin/development.php">Development</a>
	<a href="/oop/admin/design.php">Design</a>
	<a href="/oop/admin/marketing.php">Marketing</a>
	<a href="/oop/admin/human_resources.php">Human Resources</a>
	<a href="/oop/admin/finance.php">Finance</a>
	<a href="/oop/admin/sales.php">BC/Sales</a>
	<a href="/oop/admin/accounts.php">Accounts</a>
	<a href="/oop/admin/creative_strategy.php">Creative Strategy</a>
	<a href="/oop/admin/information_technology.php">Information Technology</a>
</div>

<div class="dataArea">
	<?php 
	$con=mysqli_connect("localhost","root","","oop_test");
	$result = mysqli_query($con, "SELECT  * from DEPARTMENTS where name='LeadershipDept'");
	 while ($row = mysqli_fetch_array($result)){
		echo "<h1>Leadership Team</h1>
		<form method='post' action='".htmlspecialchars($_SERVER['PHP_SELF'])."'>
			<textarea name='deptDesc'>".$row['Description']."</textarea><br/>
			<input type='submit' name='leadDesc' value='Update'/>
		</form><hr/>";
	 }	
	$result = mysqli_query($con, "SELECT  * from LEADERSHIPDEPT");
	 while ($row = mysqli_fetch_array($result)){
		$title = $row['Title'];
	echo "
	<h2>".$title."</h2>
<div class='leadLeft'>
	<form method='post' action='".htmlspecialchars($_SERVER['PHP_SELF'])."'>
		<input type='hidden' name='ID' value='".$row['ID']."'>
		<input type='hidden' name='Title' value='".$title."'>
		<label>Name:</label><br/>
			<input type='text' name='Name' value='".$row['Name']."'><br/><br/>
		<label>Job Description:</label><br/>
			<textarea name='Desc'>".$row['Description']."</textarea><br/><br/>
		<label>Job Responsibilities:</label><br/>
			<textarea name='Resp'>".$row['Responsibilities']."</textarea><br/>
		<input type='submit' name='update' value='update'/><br/>
	</form>
</div>
<div class='leadRight'>";
		showLeaderPhoto($title);
	echo "
	<form enctype='multipart/form-data' action='".htmlspecialchars($_SERVER['PHP_SELF'])."' method='POST'> 
		<input type='hidden' name='title' value='".$title."'>
		<input type='text' name='name' value='".$row['Pic']."' class='readonly' readonly>
		<input type='file' name='photo' ><br>
		<input type='submit' value='Upload Photo' name='addLeaderPic'>
	</form>
</div>	
<br class='clear'/>
<hr/>"; 
} ?> 


</div>
</div>
</div>


</body>
</html>