<?php
include 'includes/db_connect.php';
include 'includes/functions.php';
include 'includes/AdminFunctions.php';
 include_once 'includes/leadershipFunctions.php';
 
sec_session_start();
 
 if (login_check($mysqli) == true) {
    $logged = 'in';
} else {
    $logged = 'out';
}

// Check connection
if (mysqli_connect_errno()) {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
}


?>
<html>
<head>
<link rel="stylesheet" href="css/style.css" type="text/css"/>
</head>
<body>

<div class="container">
<?php include('includes/header.php');?>

<div class='block'>
	<div class='menu'>
		<a href="/oop/admin/leadership.php">Leadership</a>
		<a href="/oop/admin/development.php">Development</a>
		<a href="/oop/admin/design.php">Design</a>
		<a href="/oop/admin/marketing.php">Marketing</a>
		<a href="/oop/admin/human_resources.php">Human Resources</a>
		<a href="/oop/admin/finance.php">Finance</a>
		<a href="/oop/admin/sales.php">BC/Sales</a>
		<a href="/oop/admin/accounts.php">Accounts</a>
		<a href="/oop/admin/creative_strategy.php">Creative Strategy</a>
		<a href="/oop/admin/information_technology.php">Information Technology</a>
	</div>

<div class="dataArea">
	<h1>Welcome, Administrator!  Select a department to view and make edits</h1>

</div>
</div>
</div>

<script>

</script>

</body>
</html>