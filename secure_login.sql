-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 19, 2014 at 04:03 PM
-- Server version: 5.5.36
-- PHP Version: 5.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `secure_login`
--

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `user_id` int(11) NOT NULL,
  `time` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`user_id`, `time`) VALUES
(2, '1403032507'),
(2, '1403032743'),
(2, '1403034380'),
(2, '1403034385'),
(3, '1403130117'),
(2, '1403130154'),
(3, '1403130176');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` char(128) NOT NULL,
  `salt` char(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `username`, `email`, `password`, `salt`) VALUES
(2, 'ksheamon', 'ksheamon@gmail.com', '15251bcfec2dc8767a04b1e7a71bc9f8c30a9b5fd2cd0bdacc80827a3c3b6c4d226720988db6a31edc63f7d47063e9300010917250c6b8a13ffccbc7ecc3c3f4', 'b621e0886235efe388ed78df4b5fe3aa15a14e77a491de193d2a97b8dbcb70fe6fd813c58f39511d7defb42cf3dd1dc605da2bba93ad10a676a74c6abd8ff993'),
(3, 'test_user', 'ksheamon@bluefountainmedia.com', 'test123', ''),
(4, 'test', 'test@example.com', '68e93d1aba7e6ae8df408c222591872666b3ff8c65f0099cc5bac15d51536c786136131dd97531f73d8f741d7f5ac044dc8ffd45dd2cf7f0abc1be3b8f342cf3', 'd032eb5009fb856cf8a1b23ba484fff905a935740e4e164f361133b97ad231ad44d16db6f3ba1a217de720f1c3d3108283e9d4521adac70df9e369c5daf88f51');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
