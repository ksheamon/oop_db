-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 19, 2014 at 04:03 PM
-- Server version: 5.5.36
-- PHP Version: 5.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `oop_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
  `Name` varchar(99) NOT NULL,
  `Description` text NOT NULL,
  `HODTitle` text NOT NULL,
  `HODName` text NOT NULL,
  `HODPhoto` text NOT NULL,
  `Pic` varchar(30) NOT NULL,
  PRIMARY KEY (`Name`),
  KEY `Name` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`Name`, `Description`, `HODTitle`, `HODName`, `HODPhoto`, `Pic`) VALUES
('AccountsDept', 'The Accounts Department is made up of Account Directors and Project Managers. AD''s, PM''s and MM''s (managers) are expected to act as senior leaders and to ensure high quality work and our company is profitable.', 'Accounts HOD', 'Gary Biermann', 'biermann.png', 'biermann.png'),
('CSDept', 'The Creative Strategist is a hybrid of researcher, marketer, and designer, and works as a liaison between New Business, Accounts, Marketing, and Design Departments.', 'Creative Strategy HOD', 'Lauren Bender', '', ''),
('DesignDept', 'The Design Department is made up of Designers, Information Architects (IAs), and Video Production Specialists', 'Design/IA/Video HOD', 'Tatyana  Khamdamova', 'tatyana.png', 'tatyana.png'),
('DevDept', 'The Development team handles the programming of all sites.  They are divided into three main teams - NYC, UA,and UZ', 'Dev HOD', 'Michael Ricotta', '', 'ricotta.jpg'),
('FinanceDept', 'The Finance Department is made up of the CFO, Billing and A/R specialists, Billing Assistants, and Operations Analysts.', 'CFO/Finance HOD', 'Ralph Marino', 'marino.png', 'marino.png'),
('HRDept', 'The Human Resources Department is comprised of the HR Director, Talent Acquisition Manager, and Recruiter. They are responsible for benefits administration/management, employee relations, hiring, policy management, recruitment, and terminations.', 'Human Resources HOD', 'Samantha Lambert', 'lambert.jpg', 'lambert.jpg'),
('ITDept', '', 'Information Technology HOD', 'Ryan Brady', 'brady.jpg', 'brady.jpg'),
('LeadershipDept', 'The leadership team is comprised of the CEO, COO, CMO, and CFO/Financial Controller', '', '', '', ''),
('MarketingDept', 'The Marketing Department is divided into six sub-teams: ', 'Marketing HOD', 'Alanna Francis', 'employee.png', ''),
('SalesDept', 'The Sales Team is made up of Business Consultants (BCs) and Sales Coordinators.', 'BC/Sales HOD', 'Brian Byer', 'employee.png', '');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE IF NOT EXISTS `jobs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DeptName` varchar(99) NOT NULL,
  `Title` varchar(99) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `JobDescription` text NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Title` (`Title`),
  UNIQUE KEY `Title_2` (`Title`),
  UNIQUE KEY `Title_3` (`Title`),
  UNIQUE KEY `Title_4` (`Title`),
  UNIQUE KEY `Title_6` (`Title`),
  KEY `DeptName` (`DeptName`),
  KEY `ID` (`ID`),
  KEY `Title_5` (`Title`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`ID`, `DeptName`, `Title`, `JobDescription`) VALUES
(1, 'DevDept', 'PHP / .NET Developer', 'A developer is put to work on most projects that fall within his or her specialty content management system. They are expected to become experts in the CMS and be able to build custom modules for it, while learning to master a second open source CMS. Developers work specifically on the following types of projects:'),
(2, 'DevDept', 'HTML/CSS Coder', 'HTML Coder''s main responsibilities are creating HTML templates and applying them to existing website pages.'),
(3, 'DevDept', 'Quality Assurance Engineer', 'Quality Control Engineer is responsible for the quality of products produced by the company.'),
(4, 'DesignDept', 'Senior Designer', 'A senior designer is responsible for tackling the most challenging projects that come up in the design department.  When a junior designer needs direction on a project, a senior designer is there to provide that direction. A senior designer will be put in charge of managing the team workflow at a time when the Director of Design is away.  Senior designers work specifically on the following types of projects:'),
(5, 'DesignDept', 'Junior Designer', 'A junior designers most important role is to learn how to become a better designer. 	They will be mostly helping designers and senior designers on larger projects and gaining knowledge along the way'),
(6, 'DesignDept', 'Designer', 'A web designers work could be viewed by thousands of people every day. Web designers create the pages, layout, and graphics for web pages. Web designers may make decisions regarding what content is included on a web page, where things are placed, and how the aesthetic and continuity is maintained from one screen to the next. All of this involves skill and training in computer graphics, graphic design, and in the latest in computer and web technology'),
(7, 'DesignDept', 'Senior Information Architect (IA)', 'A Senior Information Architect is responsible for tackling the most challenging projects that come up in the Information Architecture department. A Senior IA will provide direction when an Associate IA needs direction on a project and will manage the team workflow if the Director of Information Architecture is out of the office. A Senior IA will assess existing sites for structure and usability and recommend design, implementation ideas, and efficient site planning strategies from a user-centric perspective to meet clients’ business \r\nobjectives and users’ goals. In addition to skill and training in information architecture, usability, web best practices, databases, content management systems, marketing and SEO, a Senior IA should be knowledgeable in the fields of data and information science, behavior, web development, and user interface/visual design.  Senior Information Architects work specifically on the following types of projects:'),
(8, 'DesignDept', 'Junior Information Architect (IA)', 'The Associate Information Architect’s primary role is to assist IA’s and Senior IA’s on a variety of projects in order to build up to all IA tasks and responsibilities, including assistance in client and internal meetings, client follow-ups, and meeting preparation. An Associate IA should be familiar with UX research and keep up-to-date on contemporary UX/UI practices and standards in order to provide ideation for various larger projects managed by Senior IA’s and assist in developing efficient site planning strategies and components. An Associate IA should follow the guidelines of the IA team and remain proactive and detail-oriented in all tasks<br/>\r\nAssociate Information Architects work specifically on the following types of projects:'),
(9, 'DesignDept', 'Information Architect (IA)', 'An Information Architect is responsible for the structural design of shared information environments. An IA is responsible for the organization, labeling, and navigation of information to support usability of a website. An IA is also responsible for working with project managers and web developers to design features and functionality that fit within project budgets. A Senior IA will assess existing sites for structure \r\nand usability and recommend design, implementation ideas, and efficient site planning strategies from a user-centric perspective to meet clients’ business objectives and users’ goals. An IA should be skilled and trained in information architecture, usability, web best practices, databases, content management systems, marketing and SEO\r\n<br/>\r\nInformation Architects work specifically on the following types of projects:'),
(10, 'DesignDept', 'Video Producer', 'The Video Director will prepare estimate of hours for the project with a production plan that describes the Scope of work.  Oversees all the phases of the project and works directly with ADs, BCs, Creative Strategists, Designers and Video Team ensuring high quality and unique value for the production.  The Video team works with the ADs to set Scope of Work and timeline for the project.'),
(11, 'MarketingDept', 'Marketing Specialist', ''),
(12, 'MarketingDept', 'SEO Specialist', ''),
(13, 'MarketingDept', 'Copywriter', ''),
(14, 'MarketingDept', 'SMM Specialist', ''),
(15, 'MarketingDept', 'PPC Specialist', ''),
(16, 'MarketingDept', 'Internal Marketing Associate', ''),
(17, 'HRDept', 'Director of Human Resources', ''),
(18, 'HRDept', 'Talent Acquisition Manager', ''),
(19, 'HRDept', 'Recruiter', ''),
(20, 'FinanceDept', 'Controller', ''),
(21, 'FinanceDept', 'Billing & Accounts Receivable Specialist', ''),
(22, 'FinanceDept', 'Billing Assistant', ''),
(23, 'FinanceDept', 'Operations Analyst', ''),
(24, 'SalesDept', 'Business Consultant', 'The Business Consultant(BC) is accountable to bring in new accounts to Blue Fountain Media and generate new and continuing revenue. It is important for the BC to stay in touch with their current accounts to add value and enable up-sells. '),
(25, 'SalesDept', 'Assistant Business Consultant', ''),
(26, 'SalesDept', 'Sales Coordinator', 'The Sales Coordinator is the main point of contact for all potential clients pursuing services with our company. This person makes the first impression for the company, and is accountable to screen and qualify leads, understand leads goals and present our services and capabilities to decipher leadsâ€™ fit within our team. The role is responsible for handling every in-bound sales call and scheduling meetings and consultation calls for qualified leads with the utmost courtesy and diligence.'),
(27, 'AccountsDept', 'Senior Account Director', 'Senior Account Directors are responsible for knowing the status of every project on their team and to ensure we are growing client relationships. In addition, they provide a level of oversight to a group of Account Directors to proactively address and troubleshoot problems. Finally, they serve as a mentor for Account Directors and regularly provide training and feedback on their performance.'),
(28, 'AccountsDept', 'Account Director', 'Account Directors (ADs) oversee all aspects of the project from inception to launch to ongoing marketing and retainers. ADs are responsible for the financial health of BFM by making sure the company stays profitable and grows. They work with an allocated team to ensure best results for each project, maintain profitable client relationships and upsell clients'),
(29, 'AccountsDept', 'Project Manager', ''),
(30, 'CSDept', 'Creative Strategy Manager', ''),
(31, 'ITDept', 'Systems Analyst', '');

-- --------------------------------------------------------

--
-- Table structure for table `leadershipdept`
--

CREATE TABLE IF NOT EXISTS `leadershipdept` (
  `ID` int(99) NOT NULL AUTO_INCREMENT,
  `Title` varchar(99) NOT NULL,
  `Name` varchar(99) NOT NULL,
  `Photo` text NOT NULL,
  `Description` varchar(500) NOT NULL,
  `Responsibilities` varchar(999) NOT NULL,
  `Pic` varchar(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `leadershipdept`
--

INSERT INTO `leadershipdept` (`ID`, `Title`, `Name`, `Photo`, `Description`, `Responsibilities`, `Pic`) VALUES
(1, 'CEO', 'Gabriel Shaoolian', 'gabriel.png', 'The CEO is responsible for the oversight of all day to day business at Blue Fountain Media and management of all associates.', '<li>Long-term planning of company goals</li>\r\n<li>Strategic sales</li>\r\n<li>Setting company culture</li>', 'gabriel.png'),
(2, 'COO', 'COO Name', 'employee.png', 'The COO has overall strategic and operational responsibility within Blue Fountain Media. He or she is accountable for documenting, enforcing, and streamlining all processes and setting measurable goals with each department HOD.', '<li>Ensure successful day-to-day operations at BFM</li>\r\n<li>Document all processes in a standardized, organized format</li>\r\n<li>Enforce all processes with the help of HODs</li>\r\n<li>Work with CEO, CFO, & CMO to create long-term measurable plan</li>\r\n<li>Determine organizational structure of BFM Project management & sales software</li>\r\n<li>Ensure profitability of projects and departments</li>\r\n<li>Ensure high quality of work and performance</li>\r\n<li>Project financials with the help of financial controller</li>\r\n<li>Work with HODs to consistently improve departments</li>', ''),
(3, 'CMO', 'CMO Name', 'employee.png', 'CMO heads Corporate Marketing. Overlooks all company marketing materials. Responsible for helping sales and keeping company up to date on new achievements.', '<li>Updates website with new content and projects. Each time a project is completed, update the before and after, portfolio, and case studies</li>\r\n\r\n<li>Create marketing sales tools</li>\r\n\r\n<li>Sets SEO, PPC, Social Media Marketing and other forms of marketing for BFM and measures their results.</li>\r\n\r\n<li>Provides reports weekly to CEO & sales teams.</li>\r\n\r\n<li>Comes up with new strategies</li>\r\n\r\n<li>Editorial for all public content produced by BFM</li>\r\n\r\n<li>Press relations and outreach</li>\r\n\r\n<li>Work with the BD team to plan and develop all marketing and sales materials, including demos, price sheets, trifolds, case study sheets, etc.</li>\r\n\r\n<li>Oversee the BFM newsletter</li>\r\n\r\n<li>Keep the company up to date on a weekly basis on new BFM achievements, resources and industry trends</li>', ''),
(4, 'CFO', 'Ralph Marino', '', 'The Chief Financial Officer is responsible for the financial health of the company. This role oversees all finance related operations to be sure the company is in sound financial condition. This role is also responsible for working with the finance and other departments to develop and enforce processes and procedures to guarantee funds are secure and that the books and records are complete and accurate.', '<li>Responsible for all internal and external financial reporting</li>\r\n\r\n<li>Produces monthly accrual basis Balance Sheets and Income Statements</li>\r\n\r\n<li>Prepares other cash basis reports as required by the CEO</li>\r\n\r\n<li>Prepares and updates budgets and forecasts and tracks results against budgets and forecasts</li>\r\n\r\n<li>Liaises with COO and other departments to be sure projects are kept on time and within budget</li>\r\n\r\n<li>Reviews and approves any adjustments to client billing, and ensures clients are in conformity with the financial terms of their contracts</li>\r\n\r\n<li>Supervises Billing and Collections Specialist</li>\r\n\r\n<li>Ensures company is profitable and alerts CEO, COO and departments to issues</li>\r\n\r\n<li>Handles all tax matters</li>\r\n\r\n<li>Handles banking relationships</li>\r\n\r\n<li>Responsible for 401(k) administration</li>\r\n\r\n<li>Reviews various contracts entered into by BFM</li>\r\n\r\n<li>Other tasks (permanent or ad hoc) as determined by the CEO</li>', 'marino.png');

-- --------------------------------------------------------

--
-- Table structure for table `responsibilities`
--

CREATE TABLE IF NOT EXISTS `responsibilities` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DeptName` varchar(99) NOT NULL,
  `JobTitle` varchar(99) NOT NULL,
  `Responsibilities` text NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `responsibilities`
--

INSERT INTO `responsibilities` (`ID`, `DeptName`, `JobTitle`, `Responsibilities`) VALUES
(1, 'AccountsDept', 'Project Manager', '<li>You are responsible to ensure the project is done properly and on time. You are the project manager. Being responsible means you are pro-active in making sure you get what you need on time to get the project finished in time and within budget. If you''re developer is incompetent to do something right, then you should not ask the AD for more time.</li>\r\n<li>If your developer says they finished something, check their work. Don''t take anyone''s word for it, treat the project as yours. Don''t pass blame and say QA guy didn''t do it right. At the end, you are responsible. You should treat the project as if its yours. If its your project, you would check it!</li>\r\n<li>If your developer did not do something right and leaves for the day, get them back! I don''t care if it''s a weekend and the developer has a trip planned. They cancel the trip and come back to finish the project right. This is called taking responsibility for your work. If they did not do their job properly, they should work overtime to do it. And if you fail to do this, then you are responsible. You are the manager.</li>\r\n<li>Do not pass blame at the end, you are responsible as the manager.</li>\r\n'),
(2, 'AccountsDept', 'Account Director', '<li>Show value to clients and build long-term relationships</li>\r\n<li>Set up proper expectations by hitting deadlines and informing clients when they are delaying a project</li>\r\n<li>Ensure the company remains profitable and ensure projects stay within scope of work and allocated hours.</li>\r\n<li>Make sure project quality is to our company standard.</li>\r\n<li>Ensure projects are completed in a timely manner.</li>'),
(3, 'AccountsDept', 'Senior Account Director', '<li>Ensure projects are being planned and managed appropriately</li>\r\n<li>Grow client relationships</li>\r\n<li>Proactively address problems</li>\r\n<li>Manage and grow the AD team effectively</li>\r\n<li>Detailed responsibilities</li>\r\n<li>Expectations for project management</li>\r\n<li>Execute business at quality, on time, and on budget</li>\r\n<li>Follow company policies</li>'),
(4, 'DesignDept', 'Designer ', '<li>A designer is responsible for the creation of most mid-size projects as well as contributing original ideas for various larger projects managed by Senior Designers.</li>\r\n<li>Professional website interface design.</li>\r\n<li>Logo concept art and design variations.</li>\r\n<li>Brand identity design.</li>\r\n<li>Print design.</li>\r\n<li>Assist Senior Designers with large-size projects, when necessary.</li>'),
(5, 'DesignDept', 'Video Producer', '<li>Develops the production''s vision deciding project pipeline, visual style, storyboard sequences, illustrations, design, 3D models, live action shooting, scenes, camera angles, animation, compositing, visual effects and sound design.</li>\r\n<li>Creates the hand draw storyboard, illustrations and design deciding on: look/feel, style and appropriate colors to ensure consistency with the Client''s brand and target audience. </li>'),
(6, 'DesignDept', 'Junior Designer', '<li>Add final touches to already designed interfaces.</li>\r\n<li>Create subpages using the style of a homepage designed by a more senior designer.</li>\r\n<li>Redraw logos.</li>\r\n<li>Make text corrections.</li>\r\n<li>Image work.</li>\r\n<li>Find stock photos.</li>'),
(7, 'DesignDept', 'Senior Designer', '<li>Custom website interface design.</li>\r\n<li>Wireframe design</li>'),
(8, 'DesignDept', 'Information Architect (IA)', '<li>Persona and scenario building and user flow and sitemap design</li>\r\n<li>Custom website and systems architecture</li>\r\n<li>Wireframes and prototyping</li>\r\n<li>Requirements gathering with regards to business initiatives and site features</li>\r\n<li>User experience audits</li>\r\n<li>Mobile apps and responsive websites</li>'),
(9, 'DesignDept', 'Junior Information Architect (IA)', '<li>Make edits and updates to existing wireframes and prototypes</li>\r\n<li>Create subpages that follow the home page already created by a more senior IA</li>\r\n<li>Administrative duties and assistance on projects and priorities to help the team maintain hours</li>'),
(10, 'DesignDept', 'Senior Information Architect (IA)', '<li>Persona and scenario building and user flow and sitemap design</li>\r\n<li>Custom website and systems architecture</li>\r\n<li>Wireframes and prototyping</li>\r\n<li>Requirements gathering with regards to business initiatives and site features</li>\r\n<li>User experience audits</li>\r\n<li>Mobile apps and responsive websites</li>'),
(11, 'DevDept', 'HTML/CSS Coder', '<li>Cross-browser coding</li>\r\n<li>Checking the results in main browsers</li>\r\n<li>Helping developers while applying static templates to dynamic pages</li>\r\n<li>Helping managers while estimating and planning projects</li>\r\n<li>Checking website pages after uploading updates to the servers</li>\r\n<li>Notifying managers about any problems found on the website or during the HTML coding</li>'),
(12, 'DevDept', 'PHP / .NET Developer', '<li>Projects which employ the type of CMS they are specialized in.</li>\r\n\r\n<li>Simple BFM CMS projects.</li>\r\n\r\n<li>Modules for larger projects managed by Senior Developers.</li>\r\n\r\n<li>Retainer and other maintenance projects</li>\r\n\r\n<li>Developers must also assist PMs and ADs while estimating new projects or new features.</li>'),
(13, 'DevDept', 'Quality Assurance Engineer', '<li>Control the development process and following processes to ensure the quality of their projects.</li>\r\n<li>Participate in the analysis and evaluation of the project.</li>\r\n<li>Evaluation and harmonization of time-consuming tasks required for the implementation , as well as assist the manager in the planning and evaluation tasks.</li>\r\n<li>Direct testing of the project (including test patterns, the available documentation, test and "live" version of the project).</li>\r\n<li>Participate in the analysis of project results.</li>\r\n<li>Creating and updating the necessary test documentation.</li>\r\n<li>Timely to ask the manager with all necessary information to complete the task, as well as notification of the need for additional materials necessary for testing.</li>\r\n<li>Alert managers about problems arising in the testing process.</li>\r\n<li>Effective interaction with the project team on all matters within the productive activities : working together with coder, flash programmers, developers and project managers for the implementation of the immediate tasks for testing.</li>\r\n<li>Maintain full accountability and timely notification of progress according to company standards and customer requirements, using appropriate systems.</li>\r\n<li>Timely execution of tasks in accordance with project documentation, standards and requirements of the company.</li>\r\n<li>Continuing education and professional growth.</li>'),
(14, 'FinanceDept', 'Controller', '<li>Maintains company Bookkeeping - reconciles to close each month in a timely manner</li>\r\n<li>Reviews, verifies, and pays bills (Accounts Payable)</li>\r\n<li>Processes bimonthly payroll</li>\r\n<li>Processes expense reimbursements to employees</li>\r\n<li>Cuts referral commission checks</li>\r\n<li>Calculates commissions and updates data after verifying any changes</li>\r\n<li>Record keeping in scanned and physical copies - maintains company filing system for insurance, bills, agreements</li>\r\n<li>Manages administrative staff - Reception, Office Manager, Billing Assistant</li>\r\n<li>Acts as HOD of internal team - passes forward announcements and suggesting team activities</li>\r\n<li>Expense reporting - saving copies of invoices and approvals</li>\r\n<li>Reports and follows up on any unrecognized credit card charges</li>\r\n<li>Tracks purchase payments</li>\r\n<li>Produces marketing purchase expense calculations on a monthly basis and compares to client spend amounts</li>\r\n<li>Makes purchases on behalf of clients</li>\r\n<li>Audits monthly payments received</li>\r\n<li>Assists with advanced-level invoicing questions and edits</li>\r\n<li>Backs up all internal team members as necessary</li>\r\n<li>Assists CEO as necessary</li>'),
(15, 'FinanceDept', 'Billing Assistant', '<li>Creates and sends Invoices from Daily Invoicing report</li>\r\n<li>Runs credit cards for clients</li>\r\n<li>Responds to any client emails regarding invoices sent over by following up personally or forwarding to the appropriate person</li>\r\n<li>Enters all payments received</li>\r\n<li>Creates deposits and takes them to the bank</li>\r\n<li>Audits monthly billing list for updates</li>\r\n<li>Audits invoicing in Clarizen and Quickbooks to make sure numbers match</li>'),
(16, 'FinanceDept', 'Operations Analyst', '<li>Provide support to CO</li>\r\n<li>Assist in the management, daily use, and integration of our systems for operations</li>\r\n<li>Create guides and tutorials</li>\r\n<li>Create/run company reports</li>\r\n<li>Create project/milestone templates for daily use</li>\r\n<li>Clarizen problem solving for validation errors or preventative work flow rules daily assistance</li>\r\n<li>Project handoff/Transfers</li>\r\n<li>Training</li>\r\n<li>Auditing projects, payments, and workflows</li>\r\n<li>Implementing upgrade plan for Clarizen v6</li>'),
(17, 'FinanceDept', 'Billing & Accounts Receivable Specialist', '<li>Analyze new sales contracts and set-up milestone/date related billing items in the project tracking system.</li>\r\n\r\n<li>Ensure all contracts and change orders have proper signatures.</li>\r\n\r\n<li>Work closely with Account Directors to determine when milestone based payments are due for collection.</li>\r\n\r\n<li>Contact clients proactively to insure timely payment of invoices, and follow-up on past due amounts.</li>\r\n\r\n<li>Report on receivable status aging, expected receipt dates, problem payments, etc. along with other ad-hoc reporting.\r\n\r\n<li>Audit project progress by following up on the status with each Account Director and proactively alert management of impending issues that might affect timely receipt of payments.</li>\r\n\r\n<li>Have a working knowledge of all billable items to make sure invoices are going out to clients and being paid in a timely manner.</li>\r\n\r\n<li>Work closely with the CFO to be kept abreast in the company''s finances, analyzing payments received and any other assistance needed.</li>'),
(18, 'SalesDept', 'Sales Coordinator', '<li>Serve as the first point of contact for all incoming calls, answering and transferring them to the respective staff member and taking messages when necessary.</li>\r\n<li>Serve as the main point of contact for all incoming sales leads until they have met with a senior business consultant for final pricing and closing.</li>\r\n<li>Understand each unique lead, opening the doors to create new accounts for the company.</li>\r\n<li>Know the services we offer, as well as the company portfolio, in order to effectively qualify leads and place them with the appropriate lead business consultant moving forward.</li>\r\n<li>Manage the entire sales department calendar, scheduling phone and in-person consultation meetings for all lead business consultants.</li>\r\n<li>Following consultation scheduling, provide detailed notes regarding the lead''s needs. You are held accountable to relay a detailed synopsis of all of the lead''s requests to prepare the lead business consultant for their meeting.</li>\r\n<li>Send follow-up emails after each interaction with a lead, and be in contact until the lead is ready to be passed on to a lead business consultant.</li>\r\n<li>Document and track all correspondence in the company CRM, monitoring sales performance consistently for the business development team.</li>'),
(19, 'SalesDept', 'Business Consultant', '<li>Understanding of clients business & goals</li>\r\n<li>Give valuable suggestions AND ideas</li>\r\n<li>Speak in terms of results</li>'),
(21, 'MarketingDept', 'Marketing Specialist', ''),
(22, 'MarketingDept', 'SEO Specialist', ''),
(23, 'MarketingDept', 'Copywriter', ''),
(24, 'MarketingDept', 'SMM Specialist', ''),
(25, 'MarketingDept', 'PPC Specialist', ''),
(26, 'MarketingDept', 'Internal Marketing Associate', ''),
(27, 'HRDept', 'Director of Human Resources', ''),
(28, 'HRDept', 'Talent Acquisition Manager', ''),
(29, 'HRDept', 'Recruiter', ''),
(30, 'SalesDept', 'Assistant Business Consultant', ''),
(31, 'CSDept', 'Creative Strategy Manager', ''),
(32, 'ITDept', 'Systems Analyst', '');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `jobs`
--
ALTER TABLE `jobs`
  ADD CONSTRAINT `jobs_ibfk_1` FOREIGN KEY (`DeptName`) REFERENCES `departments` (`Name`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
